package httpbinService;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** Разбирает json ответы сервиса httpbin
 */
public class HeadersAnswerParser {

    private final static String headersKeyword = "headers";

    /**
     * Метод разбирает ответ содержащий headers
     * @param json текст ответа
     * @return словарь из headers
     */
    static public Map<String, String> parse(String json)
    {
        /*
         sample:
         {
            "headers": {
                "Accept-Encoding": "gzip",
                "Connection": "close",
                "Host": "httpbin.org",
                "User-Agent": "okhttp/3.9.1"
            }
         }
         */

        HashMap<String, String> result = new HashMap<>();

        // root
        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(json);
        if (!element.isJsonObject()) {
            return result;
        }
        JsonObject mainObject = element.getAsJsonObject();
        if (!mainObject.has(headersKeyword)) {
            return result;
        }

        // headers
        JsonElement headersElement = mainObject.get(headersKeyword);
        if (!headersElement.isJsonObject()) {
            return result;
        }
        JsonObject headersObject = headersElement.getAsJsonObject();
        Set<String> keys = headersObject.keySet();

        // fields
        for (String key : keys)
        {
            result.put(key, headersObject.get(key).getAsString());
        }
        return result;
    }
}
