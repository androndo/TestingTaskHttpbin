package httpbinService.http;

import okhttp3.*;
import okhttp3.Response;

import java.io.IOException;

/**
 * Перехватывет запросы и ответы http, логгирует их в консоль
 */
public class LoggingInterceptor implements Interceptor {

    @Override public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();

        long t1 = System.nanoTime();
        System.out.println(String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.headers()));

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        System.out.println(String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers()));
        System.out.println("Is redirect: " + response.isRedirect());

        ResponseBody responseBody = response.body();
        ResponseBody copiedBody = null;
        if (responseBody != null) {
            copiedBody = deepCopyWithLog(responseBody);
        }

        if (copiedBody != null) {
            // строим заново response, иначе body не сохраняется
            return response.newBuilder().body(copiedBody).build();
        }
        return response;
    }

    // todo: refactor this
    // если выводить responseBody.string(); то потом пропадают данные,
    // поэтому пока дублируется с StoringRedirectsInterceptor.deepCopy
    private ResponseBody deepCopyWithLog(ResponseBody responseBody)
    {
        MediaType contentType = responseBody.contentType();
        String bodyString;
        try {
            bodyString = responseBody.string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        System.out.println(String.format("Body:%n%s", bodyString));
        return ResponseBody.create(contentType, bodyString);
    }
}