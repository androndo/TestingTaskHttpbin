package httpbinService.http;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;

/** Сохраняет запросы и ответы, для последующей обработки */
public class StoringRedirectsInterceptor implements Interceptor {

    private ArrayList<okhttp3.Response> responses;

    public StoringRedirectsInterceptor() {
        this.responses = new ArrayList<>();
    }

    /** Получить накопленные данные */
    public ArrayList<okhttp3.Response> getResponses() {
        return responses;
    }

    /** Очистить накопленные данные */
    public void clear() {
        responses.clear();
    }

    @Override
    public okhttp3.Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        okhttp3.Response response = chain.proceed(request);
        ResponseBody responseBody = response.body();

        if (response.isRedirect()) {
            responses.add(response);
        }

        if (responseBody != null) {
            ResponseBody body = deepCopy(responseBody);
            // строим заново response, иначе body не сохраняется
            return response.newBuilder().body(body).build();
        }
        return response;
    }

    private ResponseBody deepCopy(ResponseBody responseBody)
    {
        MediaType contentType = responseBody.contentType();
        String bodyString;
        try {
            bodyString = responseBody.string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return ResponseBody.create(contentType, bodyString);
    }
}
