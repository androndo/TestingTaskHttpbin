package httpbinService;

import httpbinService.http.LoggingInterceptor;
import httpbinService.http.StoringRedirectsInterceptor;
import okhttp3.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/** Представляет сервис http запросов и ответов httpbin
 */
public class Service {

    private StoringRedirectsInterceptor interceptor;
    private OkHttpClient client;

    private final String url = "https://httpbin.org/";
    private final String headersTail = "headers";
    private final String statusTail = "status/";
    private final String redirectTail = "redirect/";

    public Service() {
        interceptor = new StoringRedirectsInterceptor();
        client = new OkHttpClient.Builder()
                .addNetworkInterceptor(new LoggingInterceptor())
                .addNetworkInterceptor(interceptor)
                .build();
    }

    /**
     * <p>Метод передаёт заданные Http-заголовки серверу, а тот присылает их в ответ </p>
     * <p>"Returns header dict." (взято с http://httpbin.org/)</p>
     * @return Присланные заголовки
     */
    public Map<String, String> headersRequest(Map<String, String> extraHeaders) {
        Request request = prepareRequest(headersTail, extraHeaders);
        okhttp3.Response response = sendRequest(request);

        String body;
        if (response == null) {
            body = "";
        }
        else {
            body = getResponseBodyText(response);
        }
        return HeadersAnswerParser.parse(body);
    }

    /**
     * <p>Метод передаёт Http-код состояния серверу, а тот отвечает эхом, возвращая тот же статус назад.</p>
     * <p>"Returns given HTTP Status code" (взято с http://httpbin.org/)</p>
     * @param code - текст, чтобы можно было задать произвольные символы
     * @return Http-код состояния, либо -1 если ошибка.
     */
    public int statusRequest(String code) {
        Request request = prepareRequest(statusTail + code, null);
        okhttp3.Response response = sendRequest(request);
        if (response != null) {
            return response.code();
        }
        return -1;
    }

    /**
     * <p>Метод передаёт количество перенаправлений серверу, а тот присылает перенаправления n-раз</p>
     * <p>"302 Redirects n times" (взято с http://httpbin.org/)</p>
     * @return Обработанное количество перенаправлений, либо -1 если ошибка.
     */
    public int redirectRequest(int n) {
        Request request = prepareRequest(redirectTail + n, null);
        okhttp3.Response response = sendRequest(request);
        if (response == null) {
            return -1;
        }

        ArrayList<okhttp3.Response> interceptorResponses = interceptor.getResponses();
        return interceptorResponses.size();
    }

    private okhttp3.Response sendRequest(Request request) {
        interceptor.clear();
        try (okhttp3.Response response = client.newCall(request).execute()) {
            // System.out.println("response BODY = " + response.body().string()); // debug
            return response;
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Request prepareRequest(String urlTail, Map<String, String> extraHeaders) {
        Request.Builder builder = new Request.Builder();
        if (extraHeaders != null) {
            for (String h : extraHeaders.keySet()) {
                builder.addHeader(h, extraHeaders.get(h));
            }
        }
        if (urlTail != null) {
            builder.url(url + urlTail);
        }
        return builder.build();
    }

    private String getResponseBodyText(Response response) {
        ResponseBody body = response.body();
        String jsonBody = "";
        if (body != null) {
            try {
                jsonBody = body.string();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return jsonBody;
    }
}
