package httpbinService;
import io.qameta.allure.Description;

import io.qameta.allure.Step;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestSuiteHeadersEndpoint extends BaseServiceTestSuite {

    @Test
    @Description("Проверка наличия стандартных заголовков в ответе")
    void CheckDefaultHeaders() {
        ArrayList<String> defaultHeaders = new ArrayList<>(
                Arrays.asList("Accept-Encoding", "Connection", "Host", "User-Agent"));

        Map<String, String> result = SendRequestWithoutExtraHeaders();

        CheckHeadersCount(defaultHeaders, result);
        CheckHeaderNames(defaultHeaders, result);
    }

    @Test
    @Description("Помещаются дополнительные заголовки в запрос, которые должны вернуться в ответе")
    void WhenCustomHeaders_MustReturnAll()
    {
        Map<String, String> headers = new HashMap<>();
        headers.put("Ab", "cd");
        headers.put("Custom", "value");

        Map<String, String> result = SendRequestWithExtraHeaders(headers);

        CheckHeaders(headers, result);
    }

    @Test
    @Description("Переопределяется значение у стандартного заголовка, он проверяется в ответе")
    void WhenChangedDefaultHeaderValue_MustReturnIt()
    {
        String defaultHeader = "User-Agent";
        String customValue = "TestCustomValue";
        Map<String, String> headers = new HashMap<>();
        headers.put(defaultHeader, customValue);

        Map<String, String> result = SendRequestWithExtraHeaders(headers);

        CheckHeaderValue(customValue, defaultHeader, result);
    }

    @Step("Выполнение запроса без дополнительных заголовков")
    private Map<String, String> SendRequestWithoutExtraHeaders()
    {
        return service.headersRequest(null);
    }

    @Step("Проверяется совпадение размера заголовков в ответе")
    private void CheckHeadersCount(List<String> expectedHeaders, Map<String, String> actual)
    {
        assertEquals(expectedHeaders.size(), actual.size());
    }

    @Step("Проверяется присутствие полей заголовков в ответе")
    private void CheckHeaderNames(List<String> expectedHeaders, Map<String, String> actual)
    {
        for (String h : expectedHeaders)
        {
            assertTrue(actual.containsKey(h));
        }
    }

    @Step("Выполнение запроса с дополнительными заголовками")
    private Map<String, String> SendRequestWithExtraHeaders(Map<String, String> extraHeaders)
    {
        return service.headersRequest(extraHeaders);
    }

    @Step("Проверка наличия в ответе заголовков")
    private void CheckHeaders(Map<String, String> expected, Map<String, String> actual)
    {
        for (String customHeader : expected.keySet()) {
            assertTrue(actual.containsKey(customHeader));
            assertTrue(actual.containsValue(expected.get(customHeader)));
        }
    }

    @Step("Проверка значения заголовка в ответе")
    private void CheckHeaderValue(String expectedKey, String expectedValue, Map<String, String> actual)
    {
        assertEquals(expectedKey, actual.get(expectedValue));
    }



}
