package httpbinService;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class TestSuiteStatusEndpoint extends BaseServiceTestSuite {

    @Description("Проверка поддержки частых кодов состояния")
    @ParameterizedTest
    @ValueSource(ints = { 500, 404, 200 })
    void CheckStatusesEcho(int code) {
        String statusString = String.format("%d", code);
        int result = SendRequestState(statusString);
        CheckStateCodes(code, result);
    }

    @Test
    @Description("Проверка возврата ошибки когда не задан код состояния")
    void WhenEmptyCode_MustReturnNotFound() {
        int result = SendRequestState("");
        CheckStateCodes(404, result);
    }

    @Test
    @Description("Проверка возврата ошибки когда задан отрицательный код состояния")
    void WhenNegativeCode_MustReturnInternalServerError() {
        int result = SendRequestState("-1");
        CheckStateCodes(500, result);
    }

    @Step("Выполнение запроса состояния")
    private int SendRequestState(String stateCode) {
        return service.statusRequest(stateCode);
    }

    @Step("Сравнение кодов состояния")
    private void CheckStateCodes(int expected, int actual) {
        assertEquals(expected, actual);
    }

}
