package httpbinService;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TestSuiteRedirectEndpoint extends BaseServiceTestSuite {
    @Test
    @Description("Проверка тройного редиректа")
    void ThreeTimesRedirect() {
        int n = 3;
        int timesResult = SendRequestRedirect(n);
        CheckRedirectCount(n, timesResult);
    }

    @Test
    @Description("Проверка редиректа с отрицательным значением")
    void WhenNegativeRedirectNumber_ReturnsZeroRedirects() {
        int n = -1;
        int timesResult = SendRequestRedirect(n);
        CheckRedirectCount(0, timesResult);
    }

    @Step("Выполнение запроса редиректа")
    private int SendRequestRedirect(int count) {
        return service.redirectRequest(count);
    }

    @Step("Сравнение количества перенаправлений")
    private void CheckRedirectCount(int expected, int actual){
        assertEquals(expected, actual);
    }
}
